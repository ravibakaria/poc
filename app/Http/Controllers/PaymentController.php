<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Razorpay\Api\Api;
use Session;
use Redirect;

class PaymentController extends Controller
{
    public function create()
    {        
        return view('payWithRazorpay');
    }

    public function payment(Request $request)
    {
        $input = $request->all();

        $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));

        $payment = $api->payment->fetch($input['razorpay_payment_id']);
        // echo "<pre>";
        // print_r($payment);die;
        if(count($input)  && !empty($input['razorpay_payment_id'])) {
            try {
                $response = $api->payment->fetch($input['razorpay_payment_id'])->capture(array('amount'=>$payment['amount'])); 

            } catch (\Exception $e) {
                return  $e->getMessage();
                \Session::put('error',$e->getMessage());
                return redirect()->back();
            }
        }
        
        \Session::put('success', 'Payment successful');
        return redirect()->back();
    }

    public function coursesdetails($id){

        if($id == 1){
            $data = ['img' =>'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT2myIB0S8u6J4VOBsUIu7C3roHrMFCufMJfg',
            'dec' =>'The Complete JavaScript Course 2020: From Zero to Expert!',
            'shortdec' =>'The modern JavaScript course for everyone! Master JavaScript with projects, challenges and theory. Many courses in one!'];
        }
        if($id == 2){
            $data = ['img' =>'https://lh3.googleusercontent.com/proxy/xZItkFFFK3PXeStIfq0dU6YpncO4oM_3zLrYI7ZeUE1SJ5LIzWdlHim9Miyr5sUSIGTD3r1wviy6k53NXeKNlySGG9a4qdpWH_ObP_3O9o9Iv7HhTxjo9Q',
                'dec' =>'2020 Complete Python Bootcamp: From Zero to Hero in Python',
                'shortdec' =>'Learn Python like a Professional! Start from the basics and go all the way to creating your own applications and games!'
        ];
        }
        if($id == 3){
            $data = ['img' =>'https://www.php.net/images/logos/new-php-logo.svg',
            'dec' =>'PHP for Beginners - Become a PHP Master - CMS Project',
            'shortdec' =>'PHP for Beginners: learn everything you need to become a professional PHP developer with practical exercises & projects.'];
        }
        if($id == 4){
            $data = ['img' =>'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS0QYNPwmY5NhAysba6WxK6Gn8k5M02sRPy4Q',
            'dec' =>'The Complete 2020 Web Development Bootcamp',
            'shortdec' =>'Become a full-stack web developer with just one course. HTML, CSS, Javascript, Node, React, MongoDB and more!'];
        }
        if($id == 5){
            $data = ['img' =>'https://www.levelaccess.com/wp-content/uploads/2015/08/AngularJS_google.png',
            'dec' =>'Angular - The Complete Guide (2020 Edition)',
            'shortdec' =>'Master Angular 10 (formerly "Angular 2") and build awesome, reactive web apps with the successor of Angular.js'];
        }
        if($id == 6){
            $data = ['img' =>'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTevtnmA-6E1phuAYaBZEsxsIIn7H_gwyQvkw&usqp=CAU',
            'dec' =>'React Native - The Practical Guide [2020 Edition]',
            'shortdec' =>'Use React Native and your React knowledge to build native iOS and Android Apps - incl. Push Notifications, Hooks, Redux'];
        }
        return view('coursesdetails',['members'=>$data]);
    }
}
