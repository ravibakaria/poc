<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\LoginConctroller;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('ravi', function () {
    return view('payWithRazorpay');
});

Route::get('/register', [LoginConctroller::class,'login']);
Route::get('/login', [LoginConctroller::class,'index']);
Route::get('/logout', [LoginConctroller::class,'logout']);

Route::post('/submitForm', [LoginConctroller::class,'submitForm']);
Route::post('/loginaction', [LoginConctroller::class,'loginaction']);
Route::post('/submitotp', [LoginConctroller::class,'submitotp']);
Route::post('payment', [PaymentController::class, 'payment']);
Route::get('payment-razorpay', [PaymentController::class,'create']);

Route::get('courses-details/{id}', [PaymentController::class,'coursesdetails']);



// Route::get( '/login', 'Auth\Auth0IndexController@login' )->name( 'login' );
// Route::get( '/logout', 'Auth\Auth0IndexController@logout' )->name( 'logout' )->middleware('auth');