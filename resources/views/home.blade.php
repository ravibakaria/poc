@extends('common/header')
@section('content')
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
<style>
.rounded-circle {
    background-color: #c2effd;
}

@import url(https://fonts.googleapis.com/css?family=Open+Sans);
@import url(https://fonts.googleapis.com/css?family=Montserrat:700);
/* h1 {
  text-align: center;
  font-family: Montserrat,sans-serif;
  color: #333;
} */

.accordion {
  width: 100%;
  max-width: 1080px;
  height: 250px;
  overflow: hidden;
  margin: 50px auto;
}
.accordion ul {
  width: 100%;
  display: table;
  table-layout: fixed;
  margin: 0;
  padding: 0;
}
.accordion ul li {
  display: table-cell;
  vertical-align: bottom;
  position: relative;
  width: 16.666%;
  height: 250px;
  background-repeat: no-repeat;
  background-position: center center;
  transition: all 500ms ease;
}
.accordion ul li div {
  display: block;
  overflow: hidden;
  width: 100%;
}
.accordion ul li div a {
  display: block;
  height: 250px;
  width: 100%;
  position: relative;
  z-index: 3;
  vertical-align: bottom;
  padding: 15px 20px;
  box-sizing: border-box;
  color: #fff;
  text-decoration: none;
  font-family: Open Sans, sans-serif;
  transition: all 200ms ease;
}
.accordion ul li div a * {
  opacity: 0;
  margin: 0;
  width: 100%;
  text-overflow: ellipsis;
  position: relative;
  z-index: 5;
  white-space: nowrap;
  overflow: hidden;
  -webkit-transform: translateX(-20px);
  transform: translateX(-20px);
  -webkit-transition: all 400ms ease;
  transition: all 400ms ease;
}
.accordion ul li div a h2 {
  font-family: Montserrat,sans-serif;
  text-overflow: clip;
  font-size: 24px;
  text-transform: uppercase;
  margin-bottom: 2px;
  top: 160px;
}
.accordion ul li div a p {
  top: 160px;
  font-size: 13.5px;
}
.accordion ul li:nth-child(1) {
  background-image: url("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT2myIB0S8u6J4VOBsUIu7C3roHrMFCufMJfg");
}
.accordion ul li:nth-child(2) {
  background-image: url("https://lh3.googleusercontent.com/proxy/xZItkFFFK3PXeStIfq0dU6YpncO4oM_3zLrYI7ZeUE1SJ5LIzWdlHim9Miyr5sUSIGTD3r1wviy6k53NXeKNlySGG9a4qdpWH_ObP_3O9o9Iv7HhTxjo9Q");
}
.accordion ul li:nth-child(3) {
  background-image: url("https://www.php.net/images/logos/new-php-logo.svg");
}
.accordion ul li:nth-child(4) {
  background-image: url("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS0QYNPwmY5NhAysba6WxK6Gn8k5M02sRPy4Q");
}
.accordion ul li:nth-child(5) {
  background-image: url("https://www.levelaccess.com/wp-content/uploads/2015/08/AngularJS_google.png");
}
.accordion ul li:nth-child(6) {
  background-image: url("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTevtnmA-6E1phuAYaBZEsxsIIn7H_gwyQvkw&usqp=CAU");
}
.accordion ul:hover li, .accordion ul:focus-within li {
  width: 8%;
}
.accordion ul li:focus {
  outline: none;
}
.accordion ul:hover li:hover,
.accordion ul li:focus, .accordion ul:focus-within li:focus {
  width: 60%;
}
.accordion ul:hover li:hover a,
.accordion ul li:focus a, .accordion ul:focus-within li:focus a {
  background: rgba(0, 0, 0, 0.4);
}
.accordion ul:hover li:hover a *,
.accordion ul li:focus a *, .accordion ul:focus-within li:focus a * {
  opacity: 1;
  -webkit-transform: translateX(0);
  transform: translateX(0);
}
.accordion ul:hover li {
  width: 8% !important;
}
.accordion ul:hover li a * {
  opacity: 0 !important;
}
.accordion ul:hover li:hover {
  width: 60% !important;
}
.accordion ul:hover li:hover a {
  background: rgba(0, 0, 0, 0.4);
}
.accordion ul:hover li:hover a * {
  opacity: 1 !important;
  -webkit-transform: translateX(0);
  transform: translateX(0);
}

@media screen and (max-width: 600px) {
  body {
    margin: 0;
  }

  .accordion {
    height: auto;
  }
  .accordion ul li, .accordion ul li:hover, .accordion ul:hover li, .accordion ul:hover li:hover {
    position: relative;
    display: table;
    table-layout: fixed;
    width: 100%;
    -webkit-transition: none;
    transition: none;
  }
}
.about {
  text-align: center;
  font-family: 'Open Sans', sans-serif;
  font-size: 12px;
  color: #666;
}
.about a {
  color: blue;
  text-decoration: none;
}
.about a:hover {
  text-decoration: underline;
}

</style>
<div class="container-lg">
    <div class="row">
        <div class="col-md-12">
            <img src="https://img-b.udemycdn.com/notices/home_banner/image_udlite/f8324ecd-1a8d-4260-8e15-ec8aaeffa1d1.jpg?secure=_MaJmqtviugqf1faeJ4Deg%3D%3D%2C1603698770" class="img-fluid" alt="Responsive image"  height="400">


            <div class="carousel-caption">
                <h1>Example headline.</h1>
            </div>
        </div>
    </div>
    <div class="container">
        
        <div class="border-bottom pt-4"> 
        <h3>The world's largest selection of courses</h3>
        <p>Choose from 10 online video courses with new additions published every month</p>
        <h1>Ours Courses</h1>
        <div class="accordion">
            <ul>
                <li tabindex="1">
                    <div>
                        <a href="{{ url ('/courses-details/1')}}">
                        <h2>Jquery Javascript</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        </a>
                    </div>
                </li>
                <li tabindex="2">
                    <div>
                        <a href="{{ url ('/courses-details/2')}}"> 
                        <h2>Python</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        </a>
                    </div>
                </li>
                <li tabindex="3">
                    <div>
                    <a href="{{ url ('/courses-details/3')}}">
                        <h2>PHP</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        </a>
                    </div>
                </li>
                <li tabindex="4">
                    <div>
                    <a href="{{ url ('/courses-details/4')}}">
                        <h2>Web Development</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        </a>
                    </div>
                </li>
                <li tabindex="5">
                    <div>
                    <a href="{{ url ('/courses-details/5')}}">
                        <h2>Angular JS</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        </a>
                    </div>
                </li>
                <li tabindex="6">
                    <div>
                    <a href="{{ url ('/courses-details/6')}}">
                        <h2>App Development</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        </a>
                    </div>
                </li>
            </ul>
        </div>

    </div>
    <div class="row border-bottom pt-4"> 
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-2">
                    <img src="https://www.freepnglogos.com/uploads/youtube-logo-hd-8.png" height="70" width = '70' alt="" class="rounded-circle">
                </div>
                <div class="col-md-10 pl-5">
                    <h4>10 online courses</h4>
                    Enjoy a variety of fresh topics
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-2">
                <svg class="rounded-circle" viewBox="0 0 24 24" height="70" width = '70' ><path d="M20.39 19.37l-4.010-1.37-1.38 4-3.080-6-2.92 6-1.38-4-4.010 1.37 2.92-6c-0.96-1.2-1.53-2.72-1.53-4.37 0-1.857 0.737-3.637 2.050-4.95s3.093-2.050 4.95-2.050c1.857 0 3.637 0.738 4.95 2.050s2.050 3.093 2.050 4.95c0 1.65-0.57 3.17-1.53 4.37l2.92 6zM7 9l2.69 1.34-0.19 3 2.5-1.66 2.5 1.65-0.17-2.99 2.67-1.34-2.68-1.35 0.18-2.98-2.5 1.64-2.5-1.66 0.17 3.010-2.67 1.34z"></path></svg>
                </div>
                <div class="col-md-10 pl-5">
                <h4>Expert instruction</h4>
                Find the right instructor for you
                </div>
            </div>
            
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-2">
                    <img src="https://png.pngtree.com/element_our/png/20180912/infinity-logo-and-symbol-template-icons-vector-png_91838.jpg" height="70" width = '70' alt="" class="rounded-circle">
                
                </div>
                <div class="col-md-10 pl-5">
                <h4>Lifetime access</h4>
                Learn on your schedule
                </div>
            </div>
            
        </div>
    </div>
</div>
<br>
<br>
<br>
<br><br><br>
@endsection
