@extends('common/header')
@section('content')
<style>
.carousel-caption {
    
    right: 0%;
    bottom: 0px;
    left: 9%;
    z-index: 10;
    top: 39px;
    padding-bottom: 20px;
    color: #fff;
    text-align: left;
}
</style>
@if($message = Session::get('error'))
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <strong>Error!</strong> {{ $message }}
    </div>
@endif
@if($message = Session::get('success'))
    <div class="alert alert-success alert-dismissible fade {{ Session::has('success') ? 'show' : 'in' }}" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <strong>Success!</strong> {{ $message }}
    </div>
@endif
<div class="row" style ='height:400px; background-color: black;'>
    <div class="col-md-12">
        <div class="carousel-caption">
            <div class="row ">
                <div class="col-md-8">
                   
                    <h2>{{$members['dec']}}</h2>
                    <h5>{{$members['shortdec']}}</h5>
                </div>
                <div class="col-md-4">
                
                    <div class="card" style="width: 18rem;">
                        <img src="{{$members['img']}}" class="card-img-top" alt="...">
                            <div class="card-body" style = "color:black;">
                            
                           
                            <div class="card-title text-center">
                            @if(Session::get('user'))
                                <form action="{{ url('payment') }}" method="POST" >
                                    @csrf
                                    <script src="https://checkout.razorpay.com/v1/checkout.js"
                                            data-key="{{ env('RAZOR_KEY') }}"
                                            data-amount="49000"
                                            data-buttontext="Pay 490 INR"
                                            data-buttonclass="btn btn-primary"
                                            data-name="NiceSnippets"
                                            data-description="Rozerpay"
                                            data-image="{{ asset('/image/nice.png') }}"
                                            data-prefill.name="name"
                                            data-prefill.email="email"
                                            data-theme.color="#ff7529">
                                    </script>
                                </form>
                                @else
                                <a href="{{ url ('login')}}" class="btn btn-primary">Buy now</a>
                                @endif
                            </div>
                                <p class="card-text"></p>
                                
                                <p>30-Day Money-Back Guarantee</p>
                                <p>This course includes:<p>
                                <span>37.5 hours on-demand video</span><br>
                                <span> 3 articles</span><br>
                                <span>23 downloadable resources</span><br>
                                <span>Full lifetime access</span><br>
                                <span>Access on mobile and TV</span><br>
                                <span>Certificate of completion</span>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
<br><br><br>
@endsection