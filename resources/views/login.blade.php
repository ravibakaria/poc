@extends('common/header')
@section('content')

<style>
.hide{
    display:none;
}
</style>
<div class="container">
  <h2>Sign Up</h2>
  <!-- <form action="/action_page.php"> -->
  <div id="formdiv" class="show">
    <input type="hidden" id="csrf" value="{{Session::token()}}">
  <div class="form-group">
      <label for="name">Name:</label>
      <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name">
    </div>
    <div class="form-group ">
      <label for="Mobile">Mobiel:</label>
      <div class="input-box">
      <input type="text" class="fixedTxtInput" id="mobile" placeholder="Enter 10 digits Mobile Number" name="mobile">
      <span class="unit">+91</span>
      </div>
    </div>
    <div class="form-group">
      <label for="email">Email:</label>
      <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
    </div>
    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
    </div>
    
    <button id="submitForm" class="btn btn-primary">Submit</button>
</div> <!-- form div close  -->
    <div id="otpdiv" class ="hide">
    <div class="form-group">
      <label for="emotpail">OTP:</label>
      <input type="otp" class="form-control" id="otp" placeholder="Enter OTP" name="otp">
    </div>
    <button id="otpSubmit" class="btn btn-primary">Submit</button>
</div>
  <!-- </form> -->
</div>
<script>
$(function(){
    $("#submitForm").click(function(){
        var name=$("#name").val();
        var pass = $("#pwd").val();
        var mobile= $("#mobile").val();
        var email = $("#email").val();
        var token = $("#csrf").val();
        if(name ==''){
            alert("please enter name");
            return false;
        }
        if(email ==''){
            alert("please enter email");
            return false;
        }
        if(mobile ==''){
            alert("please enter mobile");
            return false;
        }
       $.ajax({
        url:"{{ url ('/submitForm')}}",
        type:'POST',
        data:{
            _token:token,
            name:name,
            mobile:mobile,
            email:email,
            pass :pass
            },
        success:function(result){
            var result = JSON.parse(result);
            // alert(result);
            if(result.statusCode == 200){
                    $("#formdiv").removeClass('show');
                    $("#formdiv").addClass('hide');
                    $("#otpdiv").removeClass('hide'); 
                    $("#otpdiv").addClass('show'); 
                    }
            else{
                alert("something wrong... please try later");
            }
        }
       })
    })

    $("#otpSubmit").click(function(){
        var otp = $("#otp").val();
        otp = otp.trim();
        var token = $("#csrf").val();
        console.log("---",otp);
        $.ajax({
            type:"POST",
            url:"{{ url ('/submitotp')}}",
            data:{
                _token:token,
                otp:otp
            },
            success:function(otpResult){
                console.log(otpResult);
                otpResult = JSON.parse(otpResult);
                
                if(otpResult.statusCode == 200){
                    location.reload();
                }
                else{
                    alert("otp not match. Please try again");
                }
            }
        })
    })
})


</script>

@endsection