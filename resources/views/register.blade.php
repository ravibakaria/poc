@extends('common/header')
@section('content')

<style>
.hide{
    display:none;
}
</style>
<div class="container">
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
  <h2>Login</h2>
  <form action="{{url('loginaction')}}" method ="POST">
    <div id="formdiv" class="show">
        <input type="hidden" id="csrf" value="{{Session::token()}}">
        @csrf
        <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
        </div>
        <div class="form-group">
        <label for="pwd">Password:</label>
        <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
        </div>
        
        <button id="submitForm" class="btn btn-primary">Submit</button>
    </div>

  </form>
</div>


@endsection